﻿namespace DiceLib.Shapes
{
    public interface IRollable<T>
    {
        T Roll();
    }
}
