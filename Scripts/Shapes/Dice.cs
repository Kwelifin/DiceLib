﻿using System;
using System.Text.RegularExpressions;
using DiceLib.RandomGenerator;

namespace DiceLib.Shapes
{
    public class Dice : IRollable<int>
    {
        private int countDices;
        public int CountDices => countDices;

        private int countEdges;
        public int CountEdges => countEdges;

        private int modifier;
        public int Modifier => modifier;


        public Dice(int countEdges)
        {
            this.countDices = 1;
            this.countEdges = countEdges;
            this.modifier = 0;
        }
        
        public Dice(int countDices, int countEdges, int modifier = 0)
        {
            this.countDices = countDices;
            this.countEdges = countEdges;
            this.modifier = modifier;
        }

        public int Roll()
        {
            return Roll(countDices, countEdges, modifier);
        }

        public static int Roll(int countDices, int countEdges, int modifier = 0)
        {
            if (countEdges <= 0)
                throw new ArgumentException("countEdges can only positive number");

            int result = modifier;
            for (int i = 0; i < countDices; i++)
            {
                result += RandomDomain.Generator.Next(1, countEdges + 1);
            }
            return result;
        }

        public static Dice Parse(string input)
        {
            if (TryParse(input, out var newDice))
                return newDice;

            throw new FormatException("Dice have invalid format. Example actual format '4d6' or 'd6'");
        }

        public static bool TryParse(string input, out Dice output)
        {
            string pattern = @"\A\s*(?<countDices>\d*)d(?<countEdges>\d+)\s*(+\s*(?<modifier>\d+))?\s*\Z";
            var regex = new Regex(pattern, RegexOptions.ExplicitCapture);
            if (regex.IsMatch(input))
            {
                var match = regex.Match(input);

                var countDiceStr = match.Groups["countDices"].Value;
                var countDices = 1;
                if (!string.IsNullOrEmpty(countDiceStr))
                    countDices = int.Parse(countDiceStr);

                var countEdgesStr = match.Groups["countEdges"].Value;
                var countEdges = int.Parse(countEdgesStr);

                var modifierStr = match.Groups["modifier"].Value;
                var modifier = 0;
                if (!string.IsNullOrEmpty(modifierStr))
                    modifier = int.Parse(modifierStr);

                output = new Dice(countDices, countEdges, modifier);
                return true;
            }
            output = null;
            return false;
        }
    }  
}
