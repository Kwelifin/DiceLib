﻿using System;
using System.Collections.Generic;
using System.Linq;
using DiceLib.RandomGenerator;

namespace DiceLib.Shapes
{
    public class WeightShape<TValue> : IRollable<TValue>
    {
        private IDictionary<TValue, int> weights;
        private int sumWeigths;

        private int CalcSumWeights()
        {
            return weights.Values.Sum();
        }

        public WeightShape(IDictionary<TValue, int> weights)
        {
            if(weights == null)
                weights = new Dictionary<TValue, int>();
            this.weights = weights;
            sumWeigths = CalcSumWeights();
        }

        public WeightShape(IList<TValue> values, IList<int> weights)
        {
            if (values.Count != weights.Count)
                throw new ArgumentException("values length != weight length");

            this.weights = new Dictionary<TValue, int>();

            var elemCount = values.Count;
            for (int i = 0; i < elemCount; i++)
            {
                this.weights.Add(values[i], weights[i]);
            }

            sumWeigths = CalcSumWeights();
        }

        public TValue Roll()
        {
            var result = RandomDomain.Generator.Next(1, sumWeigths + 1);
            foreach (var item in weights)
            {
                result -= item.Value;
                if (result <= 0)
                    return item.Key;
            }
            throw new InvalidOperationException();
        }
    }
}
