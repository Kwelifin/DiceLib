﻿namespace DiceLib.Shapes
{
    public static class Roll
    {
        public static int D2(int count = 1) => Dice.Roll(count, 2);
        public static int D4(int count = 1) => Dice.Roll(count, 4);
        public static int D6(int count = 1) => Dice.Roll(count, 6);
        public static int D8(int count = 1) => Dice.Roll(count, 8);
        public static int D10(int count = 1) => Dice.Roll(count, 10);
        public static int D12(int count = 1) => Dice.Roll(count, 12);
        public static int D20(int count = 1) => Dice.Roll(count, 20);
        public static int D100(int count = 1) => Dice.Roll(count, 100);
    }
}
