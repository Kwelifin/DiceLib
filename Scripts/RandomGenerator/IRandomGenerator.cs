﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiceLib.RandomGenerator
{
    public interface IRandomGenerator
    {
        uint NextUInt();
    }
}
