﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiceLib.RandomGenerator;

namespace DiceLib
{
    public static class RandomDomain
    {
        public static IRandomGenerator Generator { get; private set; } = new PCGRandom();

        public static void SetRandomGenerator(IRandomGenerator random)
        {
            if (random == null)
            {
                throw new ArgumentNullException();
            }

            Generator = random;
        }

        public static void SetSeed(int seed)
        {
            Generator = new PCGRandom(seed);
        }
    }
}
