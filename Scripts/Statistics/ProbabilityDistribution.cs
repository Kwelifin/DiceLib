﻿using System;
using System.Collections.Generic;
using DiceLib.Shapes;

namespace DiceLib.Statistics
{
    public static class ProbabilityDistribution
    {
        private static int[] GetRollVariantCount(int diceCount, int edgeCount)
        {
            var minRollValue = diceCount;
            var maxRollValue = diceCount*edgeCount;
            var rollSumVariantCount = maxRollValue - minRollValue + 1;
            var result = new int[rollSumVariantCount];

            if (diceCount <= 0)
            {
                throw new ArgumentException("DiceCount must be positive number");
            }

            if (diceCount == 1)
            {
                for (int i = 0; i < rollSumVariantCount; i++)
                {
                    result[i] = 1;
                }
            }
            else
            {
                var prevRollVariants = GetRollVariantCount(diceCount - 1, edgeCount);
                for (int i = 0; i < rollSumVariantCount; i++)
                {
                    result[i] = 0;
                    for (int j = -edgeCount+1; j <= 0; j++)
                    {
                        var targetIndex = i + j;
                        if (targetIndex >= 0 && targetIndex < prevRollVariants.Length)
                        {
                            result[i] += prevRollVariants[targetIndex];
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get probability distribution for dice object. Result structure (key: roll result, value: probability)
        /// </summary>
        public static Dictionary<int, float> GetDistribution(Dice dice)
        {
            var probabilityResults = new Dictionary<int, float>();
            var rolls = GetRollVariantCount(dice.CountDices, dice.CountEdges);
            var minRollValue = dice.CountDices + dice.Modifier;
            float rollVariants = (float)Math.Pow(dice.CountEdges, dice.CountDices);
            for (int i = 0; i < rolls.Length; i++)
            {
                probabilityResults.Add(i + minRollValue, rolls[i]/rollVariants);
            }

            return probabilityResults;
        }
    }
}
